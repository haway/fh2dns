#!/usr/bin/perl
#

use Getopt::Long;
use Digest::file qw(digest_file_hex);
use File::Basename;
use LWP::UserAgent;
use Data::Dumper;
use Net::DNS::Dig;
use Cwd;

$c_file = getcwd."/config.pl";

if ( -r "$c_file" ){
	require "$c_file";
}

# 1. Makeing md5 hash and put it into a hash
# 2. Chacking DNS records is exsit ?
#  2.1 NO, create it with md5
#  2.2 YES, update it with md5

my $help = undef;
my %argv;
my %filedb;
my $ua = LWP::UserAgent->new();
$ua->agent( 'fh2dns' );

$argv{debug} = undef;

GetOptions (
	"help|h" => \$help,
	"debug|d" => \$argv{debug},
	"md5|m" => \$argv{md5},
	"push|p" => \$argv{push},
	"basedir|b=s" => \$argv{basedir},
	"domain|n=s" => \$argv{domain},
	"livdnskey|l=s" => \$argv{livednskey},
	"FIlePatten|f=s" => \$argv{fpatten},
	"check|c" => \$argv{check},
	"quiet|q" => \$argv{quiet},
	"postfix|x=s" => \$argv{p_string},
);

# init
$argv{basedir} = $BASE_DIR if ( ! $argv{basedir} );
$argv{domain} = $BASE_DOMAIN if ( ! $argv{domain} );
$argv{livednskey} = $LIVEDNS_KEY if ( ! $argv{livednskey} );
$argv{p_string} = $POSTFIX_STRING if ( ! $argv{p_string} );
if ( ! $argv{fpatten} ){
	$argv{fpatten} = \@FILES_PATTEN;
}else{
	my @tmpf = split( /,/, $argv{fpatten} );
	$argv{fpatten} = \@tmpf;
}

if ( $help ){
print <<END;
Options:
	-h, --help
		Help

	-d, --debug
		Debug Mode

	-m, --md5
		Show hash tag of files

	-p, --push
		Pushing hash tag to DNS

	-x, --postfix
        Setup postfix string.

	-b, --basedir "/home/haway/tmp"
		Base Dir Path

	-n, --domain rsync.tw
		Your Domain name

	-l --livednskey I2JiedDiiwmcDWrDIWcwewEWE
		Gandi LiveDNS Key

	-f --filepatten "*.cgi,a.txt,b.config,*.php"
		Search File Patten

	-c, --check
		Checking the hash tag between local and DNS records.
END
	exit;
}

GetFile2FileDB( \%argv, \%filedb );
UpdateFileDB2DNS( \%argv, \%filedb ) if ( $argv{push} );
CheckFileAndDNS( \%argv, \%filedb ) if ( $argv{check} );

sub CheckFileAndDNS{ 
	my ( $argv, $filedb ) = @_;

	foreach my $dn ( keys %$filedb ){
		my $fqdn = "$dn$argv->{p_string}.$argv->{domain}";	# Process Domain name
		print "Checking $fqdn..." if ( $argv->{debug} );

		my $dnshash = Net::DNS::Dig->new()->for( $fqdn, 'TXT' )->rdata();
		print " Result is = $dnshash\n" if ( $argv->{debug} );

		if ( ! $dnshash ){
			print "[Error] No Hash on DNS record => $fqdn\n";
			next;
		}

		if ( $filedb->{$dn} ne $dnshash ){
			print "[Error] Hash Error; $dn files are different on $argv->{p_string} and local\n";
			print " Local is $filedb->{$dn}\n";
			print "  DNS  is $dnshash\n";
		} else {
			print "[OK] $dn - $dnshash\n" if ( ! $argv->{quiet} );
		}
	}

}

sub UpdateFileDB2DNS{ 
	my ( $argv, $filedb ) = @_;

	#curl -s -H 'X-Api-Key: I2maFeORnaxTWBEM5oHr5aP6' https://dns.beta.gandi.net/api/v5/domains/rsync.tw/records/linebot/TXT

	$ua->default_header( 'X-Api-Key' => $argv->{livednskey} );
	$ua->default_header( 'Content-Type' => 'application/json' );

	foreach my $dn ( keys %$filedb ){
		my $pdn = "$dn$argv->{p_string}";	# Process Domain name
		print "Process: $dn\n" if ( $argv->{debug} );
		my $posturl = "$LIVEDNS_SERVER/$argv->{domain}/records/$pdn/TXT";
		print " Check exist, For Test: curl -s -H 'X-Api-Key: $argv->{livednskey}' $posturl\n" if ( $argv->{debug} );

		my $response;
		$response = $ua->get( $posturl );
		if ( $response->code eq '404' ){
			# Non-exist, Create record
			print " $pdn NOT exist, creating...\n" if ( $argv->{debug} );
#curl -X POST -H "Content-Type: application/json" -H 'X-Api-Key: XXX' -d '{"rrset_ttl": 10800, "rrset_values":["<VALUE>"]}' https://dns.beta.gandi.net/api/v5/domains/<DOMAIN>/records/<NAME>/<TYPE>
		print " Check exist, For Test: curl -s -H 'X-Api-Key: $argv->{livednskey}' $posturl\n" if ( $argv->{debug} );

		#my $postdata = "{\"rrset_ttl\":300,\"rrset_values\": [\"$filedb->{$dn}\"] }";
			my $postdata = qq/{"rrset_ttl":300,"rrset_values": ["$filedb->{$dn}"] }/;

			my $req = HTTP::Request->new( 'POST', $posturl );
			$req->header( 'Content-Type' => 'application/json' );
			$req->content( $postdata );

			my $result = $ua->request( $req );

			if ( $result->code !~ /^2../i ){
				print " Error on CREATING record: $posturl, ". Dumper( $result )."\n";
			} else {
				print " Create record $pdn => $filedb->{$dn} successed!\n" if ( ! $argv->{quiet} );
			}

		}elsif ( $response->code eq '200' ){
			# Exist, Update record
			print " $pdn exist, Update...\n" if ( $argv->{debug} );
#curl -X PUT -H "Content-Type: application/json" -H 'X-Api-Key: XXX' -d '{"rrset_ttl": 10800, "rrset_values":["<VALUE>"]}' https://dns.beta.gandi.net/api/v5/domains/<DOMAIN>/records/<NAME>/<TYPE>
			my $postdata = qq/{"rrset_ttl":300,"rrset_values": ["$filedb->{$dn}"] }/;

			my $req = HTTP::Request->new( 'PUT', $posturl );
			$req->header( 'Content-Type' => 'application/json' );
			$req->content( $postdata );

			my $result = $ua->request( $req );

			if ( $result->code !~ /^2../ ){
				print " Error on CHANGING record: $posturl, ". Dumper( $result )."\n";
			} else {
				print " Change record $pdn => $filedb->{$dn} successed!\n" if (  ! $argv->{quiet} );
			}
		} else {
			print "[Error] http rcode: $response->code try -> curl -s -H 'X-Api-Key: $argv->{livednskey}' $posturl\n" ;
		}
	}

}

sub GetFile2FileDB{
	my ( $argv, $filedb ) = @_;

	print "GetFile2FileDB: Open $argv->{basedir}...\n" if ( $argv->{debug} );
	my $tdir = $argv->{basedir};

	for my $fp ( @{ $argv->{fpatten} } ){
		print " File Patten: $fp\n" if ( $argv->{debug} );
		my @sfiles = glob "$tdir/$fp";
		for ( @sfiles ){
			my $sha1file = digest_file_hex( "$_", "SHA1" );
			my ($name, $path, $suffix) = fileparse($_, qr/\.[^.]*/ );
			$suffix =~ s/\./\-/g;
			$name =~ s/_//g;
			$name = lc( $name );
			print "  $name$suffix =sha1=> $sha1file\n" if ( $argv->{debug} );
			$filedb->{ "$name$suffix" } = $sha1file;
			print "$name$suffix = $sha1file\n" if ( $argv->{md5} );
		}
	}

}



