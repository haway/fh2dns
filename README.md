fh2dns
======

File Hash 2(to) DNS txt records.

Packages
---------

p5-Net-NBsocket  

Perl Modules
--------------

use Getopt::Long;  
use Digest::SHA1;  
use File::Basename;  
use LWP::UserAgent;  
use Data::Dumper;  
use Net::DNS::Dig;  
use LWP::Protocol::https  
use Cwd;


Options
-------
<pre>
options:
	"help|h" 		
		-h, --help
		Help
	"debug|d"
		-d, --debug
		Debug Mode
	"md5|m" 
		-m, --md5
		 Doing SHA1 only
	"push|p"
		-p, --push
		Pushing hash tag to DNS records.
	"basedir|b=s"
		-b, --basedir /home/haway/tmp
		Base Dir Path
	"domain|n=s"
		-n, --domain rsync.tw
		Your Domain name
	"livdnskey|l=s" 
		-l --livednskey I2JiedDiiwmcDWrDIWcwewEWE
		Gandi LiveDNS Key
	"filepatten|f=s" 
		-f --filepatten *.cgi,a.txt,b.config,*.php
		Search File Patten
	"check|c=s" 
		-c --check
		Check hash between local files and DNS records.

</pre>
Example
---------

Server:
perl fh2dns -b /code/on/server/directory -f "*.php,a.config" -p -l "yourlivednskey" -n example.com -m

Client:
perl fh2dns -b /your/code/on/cd/server -f "*.php,a.config" -c -l "yourlivednskey" -n example.com -m


